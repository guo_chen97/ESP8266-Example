#include "stepper_28BYJ48.h"

Stepper_28BYJ48 stepper(D1, D2, D3, D4);

void setup() {
    Serial.begin(115200);
}

//转一圈512个step，一个step需要8ms，一圈4096ms（4s），每分钟15圈左右
void loop() {
    stepper.step(256);
    delay(1000);
    stepper.step(-256);
    delay(1000);
}
