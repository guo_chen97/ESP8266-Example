void setup() {
    Serial.begin(115200);
}

void loop() {
    if (!Serial.available()) return;
    String expr = Serial.readString();
    expr.trim();

    String n1, n2;
    char op;
    int result;

    int i = 0;
    // parse number 1
    while (isDigit(expr[i])) n1 += expr[i++];

    // skip space
    while (isWhitespace(expr[i])) i++;

    op = expr[i++];

    // skip space
    while (isWhitespace(expr[i])) i++;

    // parse number 2
    while (isDigit(expr[i])) n2 += expr[i++];

    switch (op)
    {
        case '+':
            result = n1.toInt() + n2.toInt();
            break;
        case '-':
            result = n1.toInt() - n2.toInt();
            break;
        case '*':
            result = n1.toInt() * n2.toInt();
            break;
        case '/':
            result = n1.toInt() / n2.toInt();
            break;
        default:
            Serial.println("input error");
            return;
    }
    Serial.println(result);
}
