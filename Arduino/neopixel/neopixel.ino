#include <Adafruit_NeoPixel.h>

Adafruit_NeoPixel pixels(12, D4, NEO_GRB + NEO_KHZ800);

void setup() {
  pixels.begin();
}

uint16_t hue = 0;

void loop() {
  for (int i = 0; i < pixels.numPixels(); i++)
  {
    //hue map 0-65535 to 0-360
    pixels.setPixelColor(i, pixels.ColorHSV(hue));
  }
  pixels.show();
  hue += 10;
  delay(1);
}
