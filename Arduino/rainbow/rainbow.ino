#include <Adafruit_NeoPixel.h>

Adafruit_NeoPixel ring(8, D4);

void setup() {
  ring.begin();
  for (int i = 0; i < 8; i++)
  {
    ring.setPixelColor(i, 0, 0, 0);
  }
  ring.show();
}

int i = 0;

void loop() {
  ring.setPixelColor((i+0)%8, 255, 0, 0); //红色
  ring.setPixelColor((i+1)%8, 255, 127, 0); //橙色
  ring.setPixelColor((i+2)%8, 255, 255, 0); //黄色
  ring.setPixelColor((i+3)%8, 0, 255, 0); //绿色
  ring.setPixelColor((i+4)%8, 0, 255, 255); //青色
  ring.setPixelColor((i+5)%8, 0, 0, 255); //蓝色
  ring.setPixelColor((i+6)%8, 255, 0, 255); //紫色
  ring.setPixelColor((i+7)%8, 255, 0, 127); //玫红
  ring.show();
  i++;
  delay(100);
}
