#include <ESP8266WiFi.h>
#include <ESPMail.h>

void setup() {
    Serial.begin(115200);
    Serial.println();
    WiFi.mode(WIFI_STA);
    WiFi.begin("众创空间", "zckj11081109");
    while(!WiFi.isConnected())
    {
        delay(100);
    }
    Serial.println(WiFi.localIP());

    ESPMail mail;
    mail.begin();
    mail.setSubject("发件人邮箱地址", "邮件标题");
    mail.addTo("收件人邮箱地址");
    mail.setBody("邮件内容");
    mail.enableDebugMode();
    if (mail.send("smtp.qq.com", 25, "QQ号", "授权码") == 0)
    {
        Serial.println("发送成功");
    }
    else
    {
        Serial.println("发送失败");
    }
    ESP.deepSleep(0);
}

void loop() {}
